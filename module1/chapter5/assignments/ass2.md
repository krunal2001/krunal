# <center>**<span style="color:blue;">Graph Traversals</span>**</center> #

# Breadth First Search (BFS)
BFS is similar to level order traversal in a tree, where we visit vertices in a level by level order. We specify a starting vertex 
_S_ and start visiting vertices of the graph _G_ from the vertex _S_. As a general graph can have cycles, we may visit the same vertex more than once. To solve this, we also maintain the state of each vertex. A vertex can be in one of three states, VISITED, NOT_VISITED, IN_PROCESS. The basic idea of breadth first search is to find the least number of edges between _S_ and any other vertex in _G_. Starting from _S_, we visit vertices of distance _k_ before visiting any vertex of distance _k+1_. For that purpose, define _ds(v)_ to be the least number of edges between _S_ and _v_ in _G_. So, for vertices _v_ that are not reachable from S we can have _ds(v)_ = ∞. We can use a queue to store vertices in progress. 
<br><br>
We also a maintain _from[u]_ for each of the vertex _u_, which denotes the vertex that discovered _u_. This information can be used to define the predecessor subgraph of _G_, which has all edges _(from[u],u)_. These edges are called as tree edges. For a tree edge _(u, v)_, _ds(v) = ds(u) + 1_. All other edges are called non-tree edges and are further classified as back edges and cross edges. A non-tree edge _(u, v)_ is called as a back edge if _ds(v)_ < _ds(u)_, meaning u can be reached from _S_ via _v_, but there is yet another shortest path through path u can be reached from _S_. An edge _(u, v)_ is called a cross edge if _d(v)_ ≤ _d(u) + 1_.
<br><br> 
_<span style="font-size:20px;">Procedure_ _BFS(G)_ </span><br>
_for each v in V(G)_ do from[v] = NIL; state[v]_ _= NOT_VISITED; d(v) = ∞__
_end-for_

_ds[S] = 0; state[S] = IN_PROCESS; from[s] = NIL;_<br>
_Q = EMPTY; Q.Enqueue(S);_

_While Q is not empty do_<br>
_v = Q.Dequeue();_<br>
_for each neighbour w of v do_<br>
_if state[w] == NOT_VISITED then_<br>
_state[w] = IN_PROCESS; from[w] = v; ds[w] = ds_[v] + 1; Q._Enqueue(w);_<br>
_end-if_<br>
_state[v] = VISITED_<br>
_end-for_<br>
_end-while_<br>
**_End-BFS_** <br><br>
<u>_Runtime:</u>Each vertex enters the queue at most once and_ _each edge is considered only once. Therefore, runtime _of_ BFS is _O(m+n)_, where _n_ is the number of vertices and _m_ is the number of edges in _G._ 

<span style="font-size:30px;">Depth First Search (DFS)</span>

The idea of DFS is to start from a specified start vertex S and explore from S as deep as possible. We go from S, to one of its neighbors x, to a neighbor of x, and so on. We stop when there are no new neighbors to explore from a given vertex. If all vertices are not visited, pick another start vertex from such vertices. We have to keep track of the state of a vertex and similar to BFS, a vertex can be in three states: VISITED, NOT_VISITED, IN_PROCESS. We also a maintain _from[u], d[u], f[u]_ for each of the vertex _u_, which denotes the vertex that discovered _u_, the discovery time of _u_ and the finish time _u_.<br><br> 
<span style="font-size:20px;">Procedure Explore(v)</span><br>
_d[v] = cur_time;_<br>
_cur_time = cur_time + 1;_<br>
_for each neighbor w of v_<br> 
_if state(w) == NOT_VISITED then_<br>
_state(w) = IN_PROCESS;_<br>
_Explore(w);_<br>
_end-if_<br>
_end-for_<br>
_state(v) = VISITED;_<br>
_f[v] = cur_time;_<br>
_cur_time = cur_time + 1;_<br>
_<span style="font-size:20px;">End-Explore_</span><br>

<span style="font-size:20px;">Procedure DFS(G)</span><br>
_cur_time = 0_<br>
_for v = 1 to n do_<br>
_if state(v) == NOT_VISITED then_<br>
_state(v) = IN_PROCESS;_<br>
_Explore(v);_<br>
_end-if_<br>
_end-for_<br>
_<span style="font-size:20px;">End-DFS_</span><br><br>
<u>Runtime:</u> Explore() is called for each vertex exactly once and each edge is considered only once. Therefore, runtime of DFS is _O(m+n)_, where _n_ is the number of vertices and _m_ is the number of edges in _G_. 

Classification of edges, based on Depth First traversal is as follows <br><br>
- <u>Tree Edges </u>: All edges _(from[u],u)_ are called as tree edges and define a dfs tree, a subgraph of _G_. <br>
   - An edge (u,v) is a tree edge if from[v] = u

- <u>Back Edges :</u> A non-tree edge (u, v) is called as a back edge if _v_ is an ancestor of _u_ in the dfs tree. _u_ can be reached from v using tree edges, but there an edge from u to v also.<br> 
  - An edge (u,v) is a back edge if [d(u), f(u)]
is a subinterval of [d(v), f(v)]

- <u>Forward Edges :</u> A non-tree edge (u, v) is called as a forward edge if _v_ is a descendant of _u_ in the dfs tree. _v_ can be reached from _u_ using tree edges, but there an edge from _u_ to _v_ also. 
  - An edge _(u,v)_ is a forward edge if [d(v), f(v)] is a subinterval of [d(u), f(u)].

- <u>Cross Edges :</u> Non-tree edges _(u, v)_ where _u_ and _v_ do not share any ancestor/descendant relationship are called cross edges. 
  - An edge _(u,v)_ is a cross edge if the two intervals [d(u), f(u)] and [d(v), f(v)] do not overlap.